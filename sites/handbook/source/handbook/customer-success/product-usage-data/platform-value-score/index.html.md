---
layout: handbook-page-toc
title: "Platform Adoption Scoring"
description: "How Platform Adoption Scoring works and ways to use it "
---

## Platform Adoption Score

### Vision

In alignment with our mission to be deliver business outcomes with GitLab's DevSecOps platform, Platform Adoption is meant to measure our customer’s deployment and usage of the application. We believe that as we help our customer’s to help customers with their digital transformation, helping them accelerate secure product development and associated business outcomes.

### Definition

Platform Adoption Score shows a customer’s Product adoption depth achieved with GitLab.

A customer’s score is the level of use cases adopted to a green health level as defined by the corresponding thresholding. Platform Adoption measures the customers usage of use cases with successful platform adoption being defined as 3+ use cases. Platform Adoption Scoring depends heavily on the [Use Case Adoption measures](/handbook/customer-success/product-usage-data/maturity-scoring/) that we calculated using metrics generated through our customer’s product usage data. A customer’s score is simply the number of Use Cases that they have adopted to a green level as defined by the corresponding thresholding.

### How we use Platform Adoption Scores

Customer Success conversations: - First and foremost, we want our customers to be successful. We believe that most of their goals will align with the value that using Gitlab as a platform can provide. As such, our CSMs use the scores as an input into how they prioritize their time with customers as well as to inform the conversations that they’re having.
Internal Key Performance Indicator: - Internally we aggregate the Platform Adoption Scores by reporting on the % of our ARR that falls within certain levels of Platform Adoption (i.e. 0, 1, 2, and 3+ Use Cases adopted).
Sales Feedback Loop: Sellers need to know how their customers are using GitLab, and how they can assist customers realize more value and provide recommendations.
Product Feedback Loop: These metrics are shared with Product to better understand the adoption of GitLab across all customers.


### References
* [Account Health Scoring](/handbook/customer-success/customer-health-scoring/#scoring-methodologies) for a list of the main measures we measure customer value
* [Use Case Adoption Methodology](/handbook/customer-success/product-usage-data/use-case-adoption/)
