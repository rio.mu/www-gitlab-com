# Job Families has been migrated

This handbook content has been migrated to the new handbook site and can now be found at:

Viewable content: [https://handbook.gitlab.com/job-families](https://handbook.gitlab.com/job-families)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/job-families](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/job-families)

If you need help or assistance with this please reach out to @jamiemaynard (Developer/Handbook) or
@marshall007 (DRI Content Sites).  Alternatively ask your questions on slack in [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)
